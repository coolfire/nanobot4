#!/usr/bin/env ruby

# Plugin that monitors the debian security advisories
class Debiansecurity
  require 'mechanize'
  require 'nokogiri'

  def initialize( status, config, output, irc, timer )
    @status     = status
    @config     = config
    @output     = output
    @irc        = irc
    @timer      = timer

    @feed_url   = 'https://www.debian.org/security/dsa-long'
    @feed_modi  = ''
    @feed_timer = 60
    @recent     = []

    @channel    = '#news'

    if( @status.threads && @config.threads)
      @rss_thread = Thread.new{ check_rss }
    end
  end

  # Default action for this plugin
  def main( nick, user, host, from, msg, arguments, con )
    
  end

  # Method to be called when the plugin is unloaded
  def unload
    if( @status.threads && @config.threads)
      @rss_thread.exit
    end
    return true
  end

  private
  def check_rss
    loop do
      begin
        agent = Mechanize.new
    
        head = agent.head( @feed_url )
        modi = head['Last-Modified']

        if(modi != @feed_modi)
          @feed_modi = modi

          # Grab rss
          xml = agent.get( @feed_url ).body
          xml = Nokogiri::XML( xml )
          # Parse out info
          title = xml.css( 'item title' ).first.text
          link  = xml.css( 'item link' ).first.text

          # Check for already announced stuff
          if @recent.include? link
            raise 'Stale RSS feed link'
          else
            @recent.push link
            if @recent.size > 10
              @recent = @recent [1..10]
            end
          end

          # If the tinyurl plugin is loaded, use it
          if( @status.checkplugin( "tinyurl" ) )
            plugin = @status.getplugin( "tinyurl" )
            link = plugin.main( nil, nil, nil, nil, nil, link, false )
          end

          @irc.message( @channel, "\x0313,1Debian\x0f #{title} | #{link}" )
          @irc.message( @channel, " " )
        end
      rescue Exception => e
        # Silently fail
        @output.debug( "Failure while retrieving rss feed: #{e.message}\n" )
      end

      # Wait for a bit before fetching again
      sleep( @feed_timer )
    end
  end
end