#!/usr/bin/env ruby

# Plugin to get dogecoin value
class Dogecoin

	require 'json'
	require 'bigdecimal'

	# This method is called when the plugin is first loaded
	def initialize( status, config, output, irc, timer )
		@status   = status
		@config   = config
		@output   = output
		@irc      = irc
		@timer    = timer
		
		@api_uri  = 'https://api.cryptonator.com/api/ticker/doge-usd'
		@last     = BigDecimal( "0.0" )
	end

	# Alias for last
	def main( _nick, _user, _host, from, _msg, _arguments, con )
		uri      = URI(@api_uri)
		response = Net::HTTP.get( uri )
puts response
		result   = JSON.parse( response )

		puts result.inspect
		# Calculate delta from last !doge
		ldiff = BigDecimal( result[ 'ticker' ][ 'price' ] ) - @last
		
		if( ldiff > 0 )
			ldiff = "+#{ldiff}"
		else
			ldiff = "#{ldiff}"
		end
		@last = BigDecimal( result[ 'ticker' ][ 'price' ] )
		
		rounded = "#{( BigDecimal( result[ 'ticker' ][ 'price' ] ) * 100 ).round / 100.0}"
		
		line = "Cryptonator DOGE/USD rate: #{rounded} (#{result[ 'ticker' ][ 'price' ]}) (#{ldiff} since last !doge)"
		
		if( con )
			@output.c( line + "\n" )
		else
			@irc.message( from, line )
		end
	end
	
	# Function to send help about this plugin (Can also be called by the help plugin.)
	def help( nick, _user, _host, _from, _msg, _arguments, con )
		help = [
			"Gets current litecoin values from cryptonator",
			"  dogecoin          - Get DOGE/USD exchange rate"
		]
	
		# Print out help
		help.each do |line|
			if( con )
				@output.c( line + "\n" )
			else
				@irc.notice( nick, line )
			end
		end
	end
end
