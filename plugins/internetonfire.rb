#!/usr/bin/env ruby

# Plugin that monitors istheinternetonfire.com
class Internetonfire
  require 'json'
  require 'httparty'

  def initialize( status, config, output, irc, timer )
    @status  = status
    @config  = config
    @output  = output
    @irc     = irc
    @timer   = timer

    @url     = 'https://istheinternetonfire.com/status.txt'
    @timer   = 3600
    @recent  = ''

    @channel = '#news'
    @prefix  = 'Tech news & security bulletins'

    if( @status.threads && @config.threads)
      @retr_thread = Thread.new{ check_state }
    end
  end

  # Default action for this plugin
  def main( nick, user, host, from, msg, arguments, con )
  end

  # Method to be called when the plugin is unloaded
  def unload
    if( @status.threads && @config.threads)
      @retr_thread.exit
    end
    return true
  end

  private
  def check_state
    loop do
      begin
        state = HTTParty.get(@url, verify: false).parsed_response

        # Check if changed since last time
        if @recent == state
          raise 'No update'
        else
          @recent = state
          # Set topic
          @irc.topic( @channel, "#{@prefix} | #{state}" )
        end

      rescue Exception => e
        # Silently fail
        @output.debug( "Error while reading status: #{e.message}\n" )
      end

      # Wait for a bit before fetching again
      sleep( @timer )
    end
  end
end
