#!/usr/bin/env ruby
# coding: utf-8

# Plugin to grab latest message from a mastodon feed
# Requires info in data/mastodon.json

require_relative '../lib/jsonapi'
require 'nokogiri'

class Mastodon

  # This method is called when the plugin is first loaded
  def initialize( status, config, output, irc, timer )
    @status  = status
    @config  = config
    @output  = output
    @irc     = irc
    @timer   = timer
    @client  = JsonApi.new('https://infosec.exchange/api/v1')

    # Start thread to retreive new tweets
    if( @status.threads && @config.threads)
      # Variables for following users
      @follow     = {}
      @filename   = "mastodon.data"
      @announce   = "#news"
      @freq       = 300
      @extra_line = true

      # Load database of users being followed
      load_db

      @ftread = Thread.new{ follow_thread }
    end
  end

  # Method to be called when the plugin is unloaded
  def unload
    if( @status.threads && @config.threads)
      @ftread.exit
    end
    return true
  end

  # Meta method for getlast
  def main( nick, user, host, from, msg, arguments, con )
    help( nick, user, host, from, msg, arguments, con )
  end

  # Method to add users to follow
  def follow( _nick, _user, host, from, _msg, arguments, con )
    if( @config.auth( host, con ) )
      if( @status.threads && @config.threads)
        if( !arguments.nil? && !arguments.empty? )
          arguments.gsub!( /&/, "" ) # Sanitize GET variables

          line = get_mastodon_statuses arguments

          unless( line.nil? )
            line = "Following: #{arguments}: #{line}"
            # Write database to disk
            write_db
          else
            line = "Error: No result."
          end
        else
          line = "Not valid input. Expecting mastodon username."
        end
      else
        line = "Follow functions not available when threading is disabled."
      end
    else
      line = "You aren't allowed to add users."
    end

    # Show output
    if( con )
      @output.c( line + "\n" )
    else
      @irc.message( from, line )
    end
  end

  # Method to delete users to follow
  def unfollow( _nick, _user, host, from, _msg, arguments, con )
    if( @config.auth( host, con ) )
      if( @status.threads && @config.threads)
        if( !arguments.nil? && !arguments.empty? )
          arguments.gsub!( /&/, "" ) # Sanitize

          # Check if user is being followed
          if( @follow.has_key?( arguments ) )
            @follow[ arguments ] = nil
            @follow.delete( arguments )
            line = "Unfollowed " + arguments + "."

            # Write database to disk
            write_db
          else
            line = "User isn't being followed."
          end
        else
          line = "Not valid input. Expecting mastodon username."
        end
      else
        line = "Follow functions not available when threading is disabled."
      end
    else
      line = "You aren't allowed to add users."
    end

    # Show output
    if( con )
      @output.c( line + "\n" )
    else
      @irc.message( from, line )
    end
  end

  # Method stop feed collection
  def stop( _nick, _user, host, from, _msg, _arguments, con )
    if( @config.auth( host, con ) )
      if( @status.threads && @config.threads)
        @ftread.exit
        line = "Feed collection stopped."
      else
        line = "Feeds are not being collected. (No threading available.)"
      end
    else
      line = "You are not authorized to perform this action."
    end

    # Show output
    if( con )
      @output.c( line + "\n" )
    else
      @irc.message( from, line )
    end
  end

  # Set channel for feeds
  def channel( _nick, _user, host, from, _msg, arguments, con )
    if( @config.auth( host, con ) )
      if( !arguments.nil? && !arguments.empty? )
        if( @status.threads && @config.threads)
          @announce = arguments
          line = "Set new channel to " + arguments + "."
        else
          line = "Feeds are not being collected. (No threading available.)"
        end
      else
        line = "Expecting channel name."
      end
    else
      line = "You are not authorized to perform this action."
    end

    # Show output
    if( con )
      @output.c( line + "\n" )
    else
      @irc.message( from, line )
    end
  end

  # Show list of accounts being followed
  def following( nick, _user, _host, _from, _msg, _arguments, con )
    line = ""
    if( @status.threads && @config.threads)
      @follow.each do |usr, _last|
        line = line + usr + " "
      end
    else
      line = "Not following anyone. (No threading.)"
    end

    if( con )
      @output.c( line + "\n" )
    else
      @irc.notice( nick, line )
    end
  end

  # Function to send help about this plugin (Can also be called by the help plugin.)
  def help( nick, _user, _host, _from, _msg, _arguments, con )
    help = [
      "This plugin retrieves the last message from a users mastodon feed.",
      "  mastodon follow [account]   - Follow mastodon user.",
      "  mastodon unfollow [account] - Unfollow mastodon user.",
      "  mastodon following          - Show list of accounts being followed.",
      "  mastodon channel [channel]  - Set channel for followed feeds.",
      "  mastodon stop               - Stop feed collection.",
      "  mastodon help               - Show this help"
    ]

    # Print out help
    help.each do |line|
      if( con )
        @output.c( line + "\n" )
      else
        @irc.notice( nick, line )
      end
    end
  end

  private

  def get_mastodon_id(accountname)
    res = @client.get('/accounts/lookup', query: "acct=#{accountname}")
    @follow[accountname] = {} unless @follow.key? accountname

    @follow[accountname]['id']       = res['id']
    @follow[accountname]['username'] = res['username']
    @follow[accountname]['last']     = ''
    write_db
  end

  def get_mastodon_statuses(accountname)
    get_mastodon_id(accountname) unless @follow.key? accountname

    id     = @follow[accountname]['id']
    res    = @client.get("/accounts/#{id}/statuses")
    status = Nokogiri::HTML(res.first['content']).text

    unless @follow[accountname]['last'] == status
      @follow[accountname]['last'] = status
      write_db
      return status
    end

    nil
  end

  # Load database from disk
  def load_db
    # Check if a database is stored on disk
    if File.exist?( @config.datadir + '/' + @filename )

      # Read database from file
      File.open( @config.datadir + '/' + @filename ) do |file|
        @follow = Marshal.load( file )
      end
    end

  end

  # Write database to disk
  def write_db
    File.open( @config.datadir + '/' + @filename, 'w' ) do |file|
      Marshal.dump( @follow, file )
    end
  end

  # Function for thread that checks updates for users being followed
  def follow_thread
    loop do
      # Loop trough users
      @follow.each do |user, data|
        begin
          line = get_mastodon_statuses(user)

          # Check for new status
          unless line.nil?
            @irc.message( @announce, "#{data['username']}: #{line}" )
            # See if an extra blank line is desired.
            @irc.message( @announce, " " ) if @extra_line
          end
        rescue Exception => e
          # Silently fail unless debugging
          @output.debug( "Failure while retreiving " + user + " feed: #{e}" )
        end
      end

      # Wait before checking for updates again
      sleep( @freq )
    end
  end
end
