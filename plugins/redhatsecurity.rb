#!/usr/bin/env ruby

# Plugin that monitors the Redhat security errata
class Redhatsecurity
  require 'mechanize'
  require 'nokogiri'

  def initialize( status, config, output, irc, timer )
    @status     = status
    @config     = config
    @output     = output
    @irc        = irc
    @timer      = timer

    @feed_url   = 'https://rhn.redhat.com/rpc/recent-errata.pxt'
    @feed_timer = 600
    @recent     = []

    @channel    = '#news'

    if( @status.threads && @config.threads)
      @rss_thread = Thread.new{ check_rss }
    end
  end

  # Default action for this plugin
  def main( nick, user, host, from, msg, arguments, con )
  end

  # Method to be called when the plugin is unloaded
  def unload
    if( @status.threads && @config.threads)
      @rss_thread.exit
    end
    return true
  end

  private
  def check_rss
    loop do
      begin
        agent = Mechanize.new

        # Grab rss
        xml = agent.get( @feed_url ).body
        xml = Nokogiri::XML( xml )

        # Parse out info
        title = xml.css( 'item title' ).last.text
        link  = xml.css( 'item link' ).last.text

        # Check for already announced stuff
        if @recent.include? link
          @recent.delete link
        else
          # If the tinyurl plugin is loaded, use it
          if( @status.checkplugin( "tinyurl" ) )
            plugin = @status.getplugin( "tinyurl" )
            link = plugin.main( nil, nil, nil, nil, nil, link, false )
          end

          @irc.message( @channel, "\x02\x034,1Red\x0f\x030,1hat\x0f #{title} | #{link}" )
          @irc.message( @channel, " " )
        end
        @recent.push link

        # Trim buffer
        @recent.shift(@recent.length - 50) if @recent.length > 50

      rescue Exception => e
        # Silently fail
        @output.debug( "Failure while retrieving rss feed: #{e.message}\n" )
      end

      # Wait for a bit before fetching again
      sleep( @feed_timer )
    end
  end
end
