#!/usr/bin/env ruby
# coding: utf-8


#Search plugin using googleapis

require 'nokogiri'
require 'mechanize'
require 'json'
require 'cgi'

class Search

	# This method is called when the plugin is first loaded
	def initialize( status, config, output, irc, timer )
		@status  = status
		@config  = config
		@output  = output
		@irc     = irc
		@timer   = timer

		@googleapiconf = 'googlesearch.json'
		@googleurl     = ''

		@isbndbapihost = "isbndb.com"
		@isbndbapipath = "/api/books.xml?access_key=%APIKEY%&index1=isbn&value1="
		@isbndbapifile = "isbndb.apikey"

		loadisbndbapikey
		loadgoogleconfig
	end

	#default method
	def main( nick, user, host, from, msg, arguments, con )
		google( nick, user, host, from, msg, arguments, con )
	end

	#This method searches google for given arguments and returns a result if found.
	def google( _nick, _user, _host, from, _msg, arguments, con )
		if( !arguments.nil? && !arguments.empty? )
			arguments.gsub!( / /, "+" ) # Sanitize search request
			# Retreive JSON
			agent = Mechanize.new
			page  = agent.get( "#{ @googleurl }#{ CGI::escape( arguments ) }" )
			json  = page.body
			res   = JSON.parse( json )
			puts res.inspect
			result = "#{res['items'][0]['link']} - #{res['items'][0]['title']}: #{res['items'][0]['snippet']}"

			if( result.empty? )
				result = "Error: No result."
			end
		else
			result = "Invalid input. Expecting search query."
		end

		if( con )
			@output.c( result + "\n" )
		else
			@irc.message( from, result )
		end
	end

	# Function to search book titles by ISBN number
	def isbn( _nick, _user, _host, from, _msg, arguments, con )
		if( !arguments.nil? && !arguments.empty? )
			arguments.gsub!( /[^0-9\-]/, "" ) # Sanitize ISBN number

			# Retreive XML
			xml = Net::HTTP.get( @isbndbapihost, @isbndbapipath + arguments )
			result = isbndbxmlparse( xml )

			if( result.empty? )
				result = "Error: No result."
			end
		else
			result = "Invalid input. Expecting search query."
		end

		if( con )
			@output.c( result + "\n" )
		else
			@irc.message( from, result )
		end
	end

	# Function to send help about this plugin (Can also be called by the help plugin.)
	def help( nick, _user, _host, _from, _msg, _arguments, con )
		help = [
			"This plugin searches something",
			"  search google [query]              - Find someting using Google",
			"  search isbn [query]                - Find book title by ISBN number",
			"  search help                        - Show this help"
		]

		# Print out help
		help.each do |line|
			if( con )
				@output.c( line + "\n" )
			else
				@irc.notice( nick, line )
			end
		end
	end

	private
	# XML parser for ISBNdb results
	def isbndbxmlparse( xmldoc )
		xmldoc = Nokogiri::XML( xmldoc )

		title = xmldoc.at_xpath( "//ISBNdb/BookList/BookData/Title" )

		if( title.instance_of? NilClass )
			return ""
		else
			return "Book title: #{title.text}"
		end
	end

	# Load API key for ISBNdb
	def loadisbndbapikey()
		# Check if a appid is stored on disk
		if File.exist?( @config.datadir + '/' + @isbndbapifile )

			# Make sure there's a variable instance
			apikey = ""

			# Read database from file
			file = File.open( @config.datadir + '/' + @isbndbapifile )

			file.each do |line|
				apikey << line
			end
			apikey.gsub!( /[:blank:\n\r]/, "" )
			file.close

			# Replace string token with actual key
			@isbndbapipath.gsub!( /%APIKEY%/, apikey )
		else
			@output.bad( "Could not load API key from #{@config.datadir}/#{@isbndbapifile} for ISBNdb search.\n" )
		end
	end
	# Load google custom search api config
	def loadgoogleconfig
		if File.exist?( @config.datadir + '/' + @googleapiconf )
			cnf = JSON.parse File.read( "#{@config.datadir}/#{@googleapiconf}" )

			@googleurl = "#{ cnf[ 'host' ] }#{ cnf[ 'path' ] }?key=#{ cnf[ 'key' ] }&num=#{ cnf[ 'num' ] }&cx=#{ cnf[ 'cx' ] }&q="
		else
			@output.bad( "Could not load API data from #{@config.datadir}/#{@googleapiconf} for Google custom search.\n" )
		end
	end
end
