#!/usr/bin/env ruby

# Plugin with misc functions for #shells
class Shells
	require 'json'

	# This method is called when the plugin is first loaded
	def initialize( status, config, output, irc, timer )
		@status  = status
		@config  = config
		@output  = output
		@irc     = irc
		@timer   = timer

		@chan    = "#shells"
	end

	# Main method
	def main( nick, _user, _host, _from, _msg, _arguments, con )
		line = "Invalid input. Use the help function to check available commands."
		if( con )
			@output.c( line + "\n" )
		else
			@irc.notice( nick, line )
		end
	end

	# Method for parsing standard messages
	def messaged( nick, user, _host, from, message )
		# Check for right channel
		if( from == @chan )
			shell_message = [
				"Hello #{nick}, please visit https://wiki.insomnia247.nl/wiki/Shells_FAQ#How_do_I_request_a_shell.3F for information on requesting shells.",
				"If you have read the FAQ already and that is why you are now here, please let us know what you are planning to use the shell for."
			]
			invite_message = [
				"Hello #{nick}, please visit https://wiki.insomnia247.nl/wiki/Shells_FAQ#How_do_I_get_an_invite_code.3F for information on getting an invite.",
				"If you have read the FAQ already and that is why you are now here, please let us know what you are planning to use the shell for."
			]

			# Looser matching for webchat users
			if( user =~ /[0-9a-f]{8}/i )
				if( message =~ /(ha[ve,z,s]|get|request).+(sh[e,3]ll)/i )
					shell_message.each { |m| @irc.message( @chan, m ) }
				elsif( message =~ /(ha[ve,z,s]|get|request).+(code|invite)/i )
					invite_message.each { |m| @irc.message( @chan, m ) }
				end
			else
				if( message =~ /(how|can|may).+(ha[ve,z,s]|get|request|invite).+(sh[e,3]ll)/i )
					shell_message.each { |m| @irc.message( @chan, m ) }
				elsif( message =~ /(how|can|may).+(ha[ve,z,s]|get|request|invite).+(code|invite)/i )
					invite_message.each { |m| @irc.message( @chan, m ) }
				end
			end
		end
	end

	# Method that receives a notification when a user joins
	def joined( nick, _user, _host, channel )
		if( channel == @chan )
			@irc.notice( nick, "Hello #{nick}, welcome to #shells for Insomnia 24/7 shell support.");
			@irc.notice( nick, "If no one is here to help you, please stick around or email your questions to coolfire\@insomnia247.nl.");

			voice = JSON.parse( Net::HTTP.get( URI.parse( "https://api.insomnia247.nl/v1/user/check/#{nick}" ) ) )
			if( voice['result'] )
				@irc.mode( channel, "+v" , nick, true )
			end
		end
	end

	# Show links
	def link( _nick, _user, _host, from, _msg, arguments, con )
		case arguments
		when /^main/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells" ]
		when /^faq/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ" ]
		when /^rule/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_rules" ]
		when /^shell/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ#How_do_I_request_a_shell.3F",
			         "The short version is: You need an invite." ]
		when /^invite/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ#How_do_I_get_an_invite.3F",
			         "The short version is: You need a good reason." ]
		when /^good/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ#What_is_a_good_reason_for_an_invite.3F" ]
		when /^bad/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ#What_are_bad_reasons_for_an_invite.3F" ]
		when /^learning/i
			info = [ "https://wiki.insomnia247.nl/wiki/Shells_FAQ#Why_isn.27t_learning_a_valid_usage.3F" ]
		when /^suwww/i
			info = [ "You can use the 'suwww' command to change to your Apache user.",
				"More info here: https://wiki.insomnia247.nl/wiki/Shells_websites#suwww" ]
		when /^heartbeat/i
			info = [ "http://heartbeat.insomnia247.nl" ]
		when /^git/i
			info = [ "https://git.insomnia247.nl",
			         "No shell account is required to sign up here." ]
		when /^port/i
		  info = [
				"Our shells have ports 5000 to 5500 open for incoming connections.",
				"The port command can be used from your shell to determime which ports are available to you.",
				"Use  port -a  to see which ports are still available.",
				"Use  port -s portnumber  to see if a specific port you would like to use is still available.",
				"For more information, go here: http://wiki.insomnia247.nl/wiki/Shells_ports"
			]
		when /^website/i
			info = [
				"Our shells allow you to host a small website on your shell.",
				"You need to put your websites files in the public_html folder on your shell.",
				"You can now view your website at yourname.insomnia247.nl.",
				"For more information, go here: http://wiki.insomnia247.nl/wiki/Shells_websites"
			]
		when /^znc/i
			info = [ "You can find a guide to setting up ZNC for your shell here: http://wiki.insomnia247.nl/wiki/Shells_ZNC"	]
		else
			info = [ "Available options: Main, FAQ, Rules, Shell, Invite, Good, Bad, Learning, suwww, Heartbeat, Git, Ports, Websites, ZNC" ]
		end
		printhelp( from, con, info )
	end

	# Alias for link
	def info( nick, user, host, from, msg, arguments, con )
		link( nick, user, host, from, msg, arguments, con )
	end

	# Aliasses for link subjects
	def port( nick, user, host, from, msg, _arguments, con )
		link( nick, user, host, from, msg, 'port', con )
	end
	def ports( nick, user, host, from, msg, _arguments, con )
		link( nick, user, host, from, msg, 'port', con )
	end
	def website( nick, user, host, from, msg, _arguments, con )
		link( nick, user, host, from, msg, 'website', con )
	end
	def websites( nick, user, host, from, msg, _arguments, con )
		link( nick, user, host, from, msg, 'website', con )
	end
	def znc( nick, user, host, from, msg, _arguments, con )
		link( nick, user, host, from, msg, 'znc', con )
	end

	# Function to send help about this plugin (Can also be called by the help plugin.)
	def help( nick, _user, _host, _from, _msg, _arguments, con )
		help = [
			"This plugin provides several functions to support " + @chan + ".",
			"  shells link [topic]    - Show link to wiki.",
			"  shells ports           - Show information about open ports.",
			"  shells websites        - Show information about users websites.",
			"  shells znc             - Show information about ZNC.",
			"  shells uptime          - Show uptime for shell hosts.",
			"  shells users           - Show user count for shell hosts.",
			"  shells online          - Show online user count for shell hosts.",
			"  shells month           - Show number of active users this month for shell hosts.",
			"  shells load            - Show load avarage for shell hosts",
			"  shells kernel          - Show kernel version for shell hosts."
		]

		# Print out help
		help.each do |line|
			if( con )
				@output.c( line + "\n" )
			else
				@irc.notice( nick, line )
			end
		end
	end

	# Stats functions
	def uptime( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "uptime" )
	end

	def users( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "users" )
	end

	def online( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "ousers" )
	end

	def month( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "numusers" )
	end

	def load( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "load" )
	end

	def kernel( _nick, _user, _host, from, _msg, _arguments, _con )
		getstat( from, "kernel" )
	end

	# Command to check if there are unvoiced users that should have voice
	def voice( _nick, _user, _host, _from, _msg, _arguments, _con )
		@irc.raw("NAMES #{@chan}")
	end

	# Capture names results
	def servermsg( _servername, messagenumber, message )
		if( messagenumber == '353' )
			parts = message.split(':')
			if(parts[0] == "#{@config.nick} = #{@chan} ")
				nicks = parts[1].split(' ')
				nicks.each do |n|
					voice = JSON.parse( Net::HTTP.get( URI.parse( "https://api.insomnia247.nl/v1/user/check/#{n}" ) ) )
					if( voice['result'] )
						@irc.mode( @chan, "+v" , n, true )
					end
				end
			end
		end
	end

	private

	# Function to grab statistics from shell hosts.
	def getstat( chan, stat )
		begin
			iline = Net::HTTP.get( URI( "https://www.insomnia247.nl/stats.php?get=#{stat}" ) )
		rescue Exception => e
			iline = "[Host appears to be down! (#{e.message})]"
		end
		#begin
			#Timeout::timeout( 10 )
		#	rline = Net::HTTP.get( 'rootedker.nl', '/stats.php?get=' + stat )
		#rescue Exception => e
		#	rline = "[Host appears to be down! (#{e.to_s})]"
		#end

		@irc.message( chan, "Insomnia 24/7: " + iline )
		#@irc.message( chan, "Rootedker.nl:  " + rline )
	end

	# Function to print arrays
	def printhelp( to, con, info )
		info.each do |line|
			if( con )
				@output.c( line + "\n" )
			else
				@irc.message( to, line )
			end
		end
	end
end
