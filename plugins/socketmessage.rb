#!/usr/bin/env ruby

# Plugin to get 
class Socketmessage

  require 'socket'
  require 'json'

  # This method is called when the plugin is first loaded
  def initialize( status, config, output, irc, timer )
    @status  = status
    @config  = config
    @output  = output
    @irc     = irc
    @timer   = timer

    @server = TCPServer.open('localhost', 2000)

    Thread.new{ startserver }
  end

  # Close server
  def unload
    @server.close
    return true
  end

  def help( nick, _user, _host, _from, _msg, _arguments, con )
    line = 'This plugin gets messages from a local socket. No interactive commands available'

    if( con )
      @output.c( line + "\n" )
    else
      @irc.notice( nick, line )
    end
  end

  private
  def startserver
    loop do
      client = @server.accept
      input  = JSON.parse client.gets
      @irc.message( input['dest'], input['message'] )
      client.close
    end
  end
end
