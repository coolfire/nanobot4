#!/usr/bin/env ruby

# Plugin to retreive information about a youtube video trough the google data API
class Youtube

	require 'net/http' # HTTP requests
	require 'json'     # JSON parsing
	require 'uri'      # URI validation

	# This method is called when the plugin is first loaded
	def initialize( status, config, output, irc, timer )
		@status     = status
		@config     = config
		@output     = output
		@irc        = irc
		@timer      = timer
		@apikeyfile = 'youtube.json'

		@api_host_video    = 'https://www.googleapis.com/youtube/v3/videos?part=%PART%&id=%ID%&maxResults=1&key=%KEY%'
		@api_host_videocat = 'https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&id=%ID%&key=%KEY%'

		load_key
	end

	# Default method
	def main( _nick, _user, _host, from, _msg, arguments, _con )
		video_id     = nil
		title        = nil
		categoryid   = nil
		category     = nil
		duration     = nil
		viewCount    = nil
		likeCount    = nil
		dislikeCount = nil

		# See what sort of link we're dealing with and extract video id accordingly
		if( arguments =~ /youtube\.com/ )
			if( arguments =~ /v=([a-zA-Z0-9\-_]{11})/ )
				video_id = $1
			end
		elsif( arguments =~ /youtu\.be\/([a-zA-Z0-9\-_]{11})/ )
			video_id = $1
		end

		# Retreive title and categoryId
		host = @api_host_video.gsub(/%ID%/, video_id)
		host.gsub!(/%PART%/, 'snippet')
		uri  = URI(host)

		Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
			request  = Net::HTTP::Get.new uri
			puts request.inspect
			response = http.request request
			puts response.inspect
			parsed   = JSON.parse( response.body )
			puts parsed.inspect

			title      = parsed['items'][0]['snippet']['title']
			categoryid = parsed['items'][0]['snippet']['categoryId']

			# Retreive duration
			host = @api_host_video.gsub(/%ID%/, video_id)
			host = host.gsub!(/%PART%/, 'contentDetails')
			uri  = URI(host)

			request  = Net::HTTP::Get.new uri
			response = http.request request
			parsed   = JSON.parse( response.body )

			duration = parsed['items'][0]['contentDetails']['duration'].gsub!(/^PT/, '').downcase

			# Retreive view and like counters
			host = @api_host_video.gsub(/%ID%/, video_id)
			host = host.gsub!(/%PART%/, 'statistics')
			uri  = URI(host)

			request  = Net::HTTP::Get.new uri
			response = http.request request
			parsed   = JSON.parse( response.body )

			viewCount    = parsed['items'][0]['statistics']['viewCount']
			likeCount    = parsed['items'][0]['statistics']['likeCount']
			dislikeCount = parsed['items'][0]['statistics']['dislikeCount']
		end

		# Retreive category for categoryId
		# You think this is enough fucking API requests already Google? Why the fuck does it need to be this much work!
		host = @api_host_videocat.gsub(/%ID%/, categoryid)
		uri  = URI(host)

		Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
			request  = Net::HTTP::Get.new uri
			response = http.request request
			parsed   = JSON.parse( response.body )

			category = parsed['items'][0]['snippet']['title']
		end

		result = "Category: #{category} | Length: #{duration} | Views: #{viewCount} | Like/dislike: #{likeCount}/#{dislikeCount}"
		@irc.message( from, result )
	end

	# Download video and transcode
	def mp3( _nick, _user, _host, from, _msg, arguments, _con )
		# Check for valid url
		if( arguments =~ /\A#{URI::DEFAULT_PARSER.make_regexp(['http', 'https'])}\z/ )

			# Check for youtube url
			if( arguments =~ /youtu(be\.com|\.be)/ )

				# Send to youtube-dl
				@irc.message( from, "Working..." )
				cmd = "youtube-dl --output '~/downloads/%(title)s.%(ext)s' --no-call-home --restrict-filenames --no-color --extract-audio --audio-format mp3 #{arguments}"
				res = %x(#{cmd})

				# Parse output
				if( res =~ /\[ffmpeg\] Destination: (.+\.mp3)/ )
					file = $1
					if( file =~ /([^\/]+\.mp3)$/ )
						@irc.message( from, "Available here for 10 minutes: http://149.56.100.2/#{$1}" )
					end
					@timer.action( 600, "%x(rm #{file})" )
				else
					@irc.message( from, "Could not get mp3 file." )
				end
			else
				@irc.message( from, "No support for non-youtube URLs at this time." )
			end
		else
			@irc.message( from, "This does not appear to be a valid URL." )
		end
	end

	# Function to send help about this plugin (Can also be called by the help plugin.)
	def help( nick, _user, _host, _from, _msg, _arguments, con )
		help = [
			"Plugin to retreive data from youtube videos",
			"  youtube url                 - Grab data for youtube video.",
			"  youtube mp3                 - Grab the youtube video and transcode it to mp3."
		]

		# Print out help
		help.each do |line|
			if( con )
				@output.c( line + "\n" )
			else
				@irc.notice( nick, line )
			end
		end
	end

	private

	# Load API key from json file and put it in the URLs
	def load_key
		if File.exist?( @config.datadir + '/' + @apikeyfile )

			jsonline = ""
			File.open( @config.datadir + '/' + @apikeyfile ) do |file|
				file.each do |line|
					jsonline << line
				end
			end
			parsed = JSON.parse( jsonline )

			apikey = parsed[ 'apikey' ]
			@api_host_video.gsub!(/%KEY%/, apikey)
			@api_host_videocat.gsub!(/%KEY%/, apikey)
		end
	end
end
