#!/usr/bin/env ruby

jruby = ARGV.length == 1
ret   = 0

require 'jruby' if jruby

Dir["**/*.rb"].each do |f|
  res = String.new  
  if jruby
    begin
      JRuby.parse(File.read(f))
      res = 'Syntax OK'
    rescue SyntaxError => e
      puts e
    end
  else
    res = `ruby -c #{f}`
  end

  if res.empty?
    ret = 1
    puts "#{f} FAILED!"
  else
    puts "#{f.ljust(30)} #{res.rjust(30)}"
  end
end

exit ret

